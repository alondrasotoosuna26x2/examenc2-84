const express = require("express");
const router = express.Router();
const bodyparser = require("body-parser"); 

router.get("/recibo", (req, res)=>{
    const valores ={
        numRecibo:req.query.numRecibo,
        nombre:req.query.nombre,
        domicilio:req.query.domicilio, 
        tipoServicio:req.query.tipoServicio,
        kilowatts:req.query.kilowatts,
        subtotal:req.query.subtotal,
        impuesto:req.query.impuesto, 
        descuento:req.query.descuento, 
        total:req.query.total
        
    }
    res.render('recibo.html', valores); 
})
router.get("/resultados", (req, res)=>{
    const valores ={
        numRecibo:req.query.numRecibo,
        nombre:req.query.nombre,
        domicilio:req.query.domicilio, 
        tipoServicio:req.query.tipoServicio,
        kilowatts:req.query.kilowatts,
        subtotal:req.query.subtotal,
        impuesto:req.query.impuesto, 
        descuento:req.query.descuento, 
        total:req.query.total
        
    }
    res.render('resultados.html', valores); 
})
router.post("/recibo", (req, res)=>{
    const valores ={
        numRecibo:req.body.numRecibo, 
        nombre:req.body.nombre,
        domicilio:req.body.domicilio, 
        tipoServicio:req.body.tipoServicio,
        kilowatts:req.body.kilowatts,
        subtotal:req.body.subtotal,
        impuesto:req.body.impuesto, 
        descuento:req.body.descuento, 
        total:req.body.total
    }
    res.render('recibo.html', valores); 
})

router.post("/resultados", (req, res)=>{
    const valores ={
        numRecibo:req.body.numRecibo, 
        nombre:req.body.nombre,
        domicilio:req.body.domicilio, 
        tipoServicio:req.body.tipoServicio,
        kilowatts:req.body.kilowatts,
        subtotal:req.body.subtotal,
        impuesto:req.body.impuesto, 
        descuento:req.body.descuento, 
        total:req.body.total
    }
    res.render('resultados.html', valores); 
})
function limpiarCampos() {
    document.getElementById("numRecibo").value = "";
    document.getElementById("nombre").value = "";
    document.getElementById("domicilio").value = "";
    document.getElementById("edad").value = "";
    document.getElementById("tipoServicio").value = "";
    document.getElementById("kilowatts").value = "";
}

module.exports=router;