const http = require("http");
const express = require("express");
const bodyparser = require("body-parser");
const misRutas = require("./router/index");
const path = require("path"); 

const app = express ();
app.set('view engine', "ejs")
app.use(express.static(__dirname + '/public'));
app.engine('html', require('ejs').renderFile); 
app.use(bodyparser.urlencoded({extended:true}));
app.use(misRutas);
//declarar un arrays de objetos

//res.send("<h1> iniciamos con express </h1>"); 

app.use((req,res, next)=>{
    res.status(404).sendFile(__dirname+ '/public/error.html');
})
//escuchar el servidor por el puerto x
const puerto = 4002;
app.listen(puerto,()=>{
    console.log("iniciado puerto 4002");
})